# zero-server

Get a full fake REST API with zero coding in less than 30 seconds (seriously), based on https://github.com/typicode/json-server. 

PLUS a CHAOS MONKEY, https://github.com/Netflix/SimianArmy/wiki/Chaos-Monkey

## Getting started

Start here: https://github.com/typicode/json-server, in short:
1. Install json-server: ```npm install -g json-server```
2. Run the server: ```json-server --watch aag.json --middlewares chaos-monkey.js```
3. Your mock server runs at: ```  http://localhost:3000```

## Configure the Chaos Monkey: chaos-monkey.js

This is pretty much self-explanatory:

```
module.exports = (req, res, next) => {
    var items = Array(300, 301, 302, 303, 400, 401, 403, 404, 500);
    var code = items[Math.floor(Math.random()*items.length)];
    var chance = Math.random() * 100;
    if (chance < 25) { // <- This is the CHANCE where the non-200 status code will be sent to the client
        res.sendStatus(code);
    } else {
        next();
    }
}
```



