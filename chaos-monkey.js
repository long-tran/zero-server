module.exports = (req, res, next) => {
    var items = Array(300, 301, 302, 303, 400, 401, 403, 404, 500);
    var code = items[Math.floor(Math.random()*items.length)];
    var chance = Math.random() * 100;
    if (chance < 25) {
        res.sendStatus(code);
    } else {
        next();
    }
}