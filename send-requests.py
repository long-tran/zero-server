
import requests 
  
URL = "http://localhost:3000/posts"
PARAMS = {} 
TRIES = 100
c = 0

for i in range(100):
    r = requests.get(url = URL, params = PARAMS) 
    #print("Status code: %s" %(r.status_code))
    if r.status_code < 300:
        c = c + 1

print("Success rate: %s" %(c*100/TRIES))